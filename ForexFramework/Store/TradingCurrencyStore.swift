//
//  TradingCurrencyStore.swift
//  RestClient
//
//  Created by Emmanuel Tugado on 28/11/18.
//  Copyright © 2018 Emmanuel Tugado. All rights reserved.
//

import Foundation

public protocol DataStore {
    associatedtype T: NSObject

    var dataCache: NSCache<NSString, T> { get }
    var dataKey: NSString { get }

    func saveCurrencies(_ currencies: [TradingCurrency])
    func getCurrencies() -> [TradingCurrency]
}

public class TradingCurrencyStore: DataStore {
    public let dataCache = NSCache<NSString, NSArray>()
    public let dataKey = "TradingCurrencies" as NSString

    public init() {}

    public func saveCurrencies(_ currencies: [TradingCurrency]) {
        dataCache.setObject(currencies as NSArray, forKey: dataKey)
    }

    public func getCurrencies() -> [TradingCurrency] {
        guard let currencies = dataCache.object(forKey: dataKey) as? [TradingCurrency] else { return [] }
    
        return currencies
    }
}
