//
//  TradingCurrency.swift
//  RestClient
//
//  Created by Emmanuel Tugado on 28/11/18.
//  Copyright © 2018 Emmanuel Tugado. All rights reserved.
//

import Foundation

public class TradingCurrency: NSObject, Decodable {
    public let code: String
    public let name: String
    public let country: String
    public let lastUpdated: String

    public let buyQuoteStr: String
    public let sellQuoteStr: String

    public var buyQuote: Double? {
        return Double(buyQuoteStr)
    }

    public var sellQuote: Double? {
        return Double(sellQuoteStr)
    }

    public var hasValidQuotes: Bool {
        return buyQuote != nil && sellQuote != nil
    }

    public var displayText: String {
        return "\(code.uppercased()) - \(name.capitalized) - \(country.capitalized)"
    }

    enum CodingKeys: String, CodingKey {
        case code = "currencyCode"
        case name = "currencyName"
        case country
        case buyQuoteStr = "buyTT"
        case sellQuoteStr = "sellTT"
        case lastUpdated = "LASTUPDATED"
    }

    required public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)

        code = try values.decode(String.self, forKey: .code)
        name = try values.decode(String.self, forKey: .name)
        country = try values.decode(String.self, forKey: .country)
        buyQuoteStr = try values.decode(String.self, forKey: .buyQuoteStr)
        sellQuoteStr = try values.decode(String.self, forKey: .sellQuoteStr)
        lastUpdated = try values.decode(String.self, forKey: .lastUpdated)
    }
}

extension TradingCurrency: Comparable {
    public static func < (lhs: TradingCurrency, rhs: TradingCurrency) -> Bool {
        return lhs.code.lowercased() < rhs.code.lowercased()
    }

    public static func == (lhs: TradingCurrency, rhs: TradingCurrency) -> Bool {
        return lhs.code.lowercased() == rhs.code.lowercased() &&
            lhs.country.lowercased() == rhs.country.lowercased()
    }
}

