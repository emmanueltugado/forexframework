//
//  URL+Extension.swift
//  RestClient
//
//  Created by Emmanuel Tugado on 26/11/18.
//  Copyright © 2018 Emmanuel Tugado. All rights reserved.
//

import Foundation

public extension URL {
    public init?(path: String, params: [String: Any], method: HTTPRequestMethod) {
        guard var urlComps = URLComponents(string: path),
            let generatedURL = urlComps.url
        else { return nil }

        switch method {
        case .get, .delete:
            urlComps.queryItems = params.compactMap { URLQueryItem(name: $0.key, value: $0.value as? String) }
        default: break
        }

        self = generatedURL
    }
}
