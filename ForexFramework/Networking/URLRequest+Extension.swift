//
//  URLRequest+Extension.swift
//  RestClient
//
//  Created by Emmanuel Tugado on 24/11/18.
//  Copyright © 2018 Emmanuel Tugado. All rights reserved.
//

import Foundation

public extension URLRequest {
    public init?(path: String, params: [String: Any], method: HTTPRequestMethod) {
        guard let url = URL(path: path, params: params, method: method) else { return nil }

        self.init(url: url)

        httpMethod = method.rawValue
        setValue("application/json", forHTTPHeaderField: "Accept")
        setValue("application/json", forHTTPHeaderField: "Content-Type")

        switch method {
        case .post, .patch, .put:
            httpBody = try? JSONSerialization.data(withJSONObject: params, options: .sortedKeys)
        default: break
        }
    }
}
