//
//  APIClient.swift
//  RestClient
//
//  Created by Emmanuel Tugado on 26/11/18.
//  Copyright © 2018 Emmanuel Tugado. All rights reserved.
//

import Foundation

public enum RequestResult<T> {
    case success(T)
    case failure(Error)

    public func parseValue() throws -> T {
        switch self {
        case .success(let value): return value
        case .failure(let error): throw error
        }
    }
}

public protocol APIClient {
    var baseURL: String { get }

    func sendRequest(
        to path: String,
        params: [String: Any],
        method: HTTPRequestMethod,
        completionHandler: ((RequestResult<Data>) -> Void)?
    )
}

public class WestpacAPIClient: APIClient {
    public let baseURL = "https://www.westpac.com.au/"

    public init() {}

    public func sendRequest(
        to path: String,
        params: [String : Any] = [:],
        method: HTTPRequestMethod,
        completionHandler: ((RequestResult<Data>) -> Void)?
    ) {
        guard let request = URLRequest(path: path, params: params, method: method)
        else {
            let error = NSError(domain: "Cannot construct URLRequest", code: 999, userInfo: nil)
            DispatchQueue.main.async { completionHandler?(.failure(error)) }

            return
        }

        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            guard let httpResp = response as? HTTPURLResponse, (200...299) ~= httpResp.statusCode else { return }

            if let responseData = data {
                DispatchQueue.main.async {
                    completionHandler?(.success(responseData) )
                }
            }
        }

        task.resume()
    }
}

public final class ForexAPIClient: WestpacAPIClient {
    private enum NestedKeys: String {
        case data = "data"
        case brands = "Brands"
        case wbc = "WBC"
        case portfolios = "Portfolios"
        case fx = "FX"
        case products = "Products"
    }

    public func getForexRates(completionHandler: ((RequestResult<[TradingCurrency]>) -> Void)?) {
        let fullURL = baseURL + "bin/getJsonRates.wbc.fx.json"

        sendRequest(to: fullURL, method: .get) { result in
            do {
                let data = try result.parseValue()

                guard let respDict = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any],
                    let respData = respDict[NestedKeys.data.rawValue] as? [String: Any],
                    let brandsData = respData[NestedKeys.brands.rawValue] as? [String: Any],
                    let wbc = brandsData[NestedKeys.wbc.rawValue] as? [String: Any],
                    let portfolios = wbc[NestedKeys.portfolios.rawValue] as? [String: Any],
                    let fx = portfolios[NestedKeys.fx.rawValue] as? [String: Any],
                    let products = fx[NestedKeys.products.rawValue] as? [String: Any],
                    let currencyData = Array(products.values) as? [[String: Any]]
                else {
                    let error = NSError(domain: "Cannot parse response", code: 998, userInfo: nil)
                    DispatchQueue.main.async { completionHandler?(.failure(error)) }

                    return
                }

                let ratesData: [[String: [String: Any]]] = currencyData.compactMap { $0["Rates"] as? [String: [String: Any]] }
                let ratesValues = ratesData.compactMap { $0.values.first }

                guard let ratesJSON = try? JSONSerialization.data(withJSONObject: ratesValues, options: .sortedKeys),
                    let parsedRates = try? JSONDecoder().decode([TradingCurrency].self, from: ratesJSON)
                else {
                    let error = NSError(domain: "Cannot parse response", code: 998, userInfo: nil)
                    DispatchQueue.main.async { completionHandler?(.failure(error)) }

                    return
                }

                completionHandler?(.success(parsedRates))
            } catch let error {
                completionHandler?(.failure(error))
            }
        }
    }
}
