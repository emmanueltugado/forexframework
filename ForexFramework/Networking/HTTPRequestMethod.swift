//
//  HTTPRequestMethod.swift
//  RestClient
//
//  Created by Emmanuel Tugado on 24/11/18.
//  Copyright © 2018 Emmanuel Tugado. All rights reserved.
//

import Foundation

public enum HTTPRequestMethod: String {
    case get = "GET"
    case post = "POST"
    case patch = "PATCH"
    case put = "PUT"
    case delete = "DELETE"
}

